-- use music_db
USE music_db;

-- artists with letter "d" in the name
SELECT * FROM artists WHERE name LIKE "%d%";

-- songs that has a length of less than 230
SELECT * FROM songs WHERE length<230;

-- join albums and songs table with only the album_name,song_name and song length
SELECT albums.album_title, songs.song_name,songs.length FROM albums
    JOIN songs ON songs.album_id=albums.id;

-- join artists and songs table (find all albums with letter "a")
SELECT * FROM albums 
    RIGHT JOIN artists ON albums.artist_id=artists.id
    WHERE album_title LIKE "%a%";

-- sort albums in descending order
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the albums and songs tables
SELECT albums.album_title,songs.song_name FROM albums
    JOIN songs ON albums.id=songs.album_id
    ORDER BY album_title DESC, song_name ASC;